window.onload = function () {

  // Smooth scroll to the form
  const btn = document.querySelector('[href="#form"]');
  btn.addEventListener(
    'click',
    function (e) {
      form.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
      });
    },
    true
  );

  // Form validation
  const form = document.getElementById('form');
  let inputs = document.querySelectorAll('.check-input');
  // name of the form
  let elements = forma.elements;
  let modal = document.getElementById('modal');
  let overlay = document.getElementById('overlay');
  let email = document.getElementById('email');
  let tel = document.getElementById('tel');

  // Close Modal pop-up
  overlay.addEventListener('click', function () {
    overlay.classList.remove('overlay-show');
    modal.classList.remove('modal-show');
    clearForm();
  });

  // remove class "error" as type
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].oninput = function () {
      inputs[i].classList.remove('error');
    };
  }

  form.onsubmit = function (e) {
    let error = false;
    e.preventDefault();
    for (let i = 0; i < inputs.length; i++) {
      if (inputs[i].value === '') {
        inputs[i].classList.add('error');

        error = true;
      } else {
        // e.preventDefault();
        inputs[i].classList.remove('error');
        modal.classList.add('modal-show');
        overlay.classList.add('overlay-show');
        // очищаем поля формы
        clearForm();
      }
    }
  }
  if (!ValidMail()) {
    email.classList.add('error');
    error = true;
  } else {
    email.classList.remove('error');
  }
  if (!ValidPhone()) {
    tel.classList.add('error');
    error = true;
  } else {
    tel.classList.remove('error');
  }

  if (error) {
    modal.classList.remove('modal-show');
    overlay.classList.remove('overlay-show');
  }


  // TODO Name validation, need for revision, rework, refactoring
  // let el = document.getElementById('name');
  // el.addEventListener('blur', checkName, false);
  // function checkName() {
  //   let elMsg = document.getElementById('feedback');
  //   if (this.value.length < 2) {
  //     elMsg.textContent = 'Имя слишком короткое';
  //   } else {
  //     elMsg.textContent = '';
  //   }
  // }
  function checkUsername() { // Объявляем функцию
    var nameLastname = el.value; // Сохраняем имя пользователя в переменной
    if (nameLastname.length < 3) { // Если в имени пользователя меньше 5 символов
      elMsg.className = 'error'; // Изменяем атрибут class у этого сообщения
      elMsg.textContent = 'Имя слишком короткое...'; // Стираем сообщение
    } else { // Иначе
      elMsg.textContent = ''; // Обновляем сообщение
    }
  }
  function tipUsername() { // Объявляем функцию сообщения условия
    elMsg.className = 'tip'; // Меняем у сообщения атрибут class
    elMsg.innerHTML = 'Имя пользователя должно содержать не менее 5 символов'; // Добавляем сообщение
  }
  let el = document.getElementById('username'); // Получаем введенное имя пользователя
  let elMsg = document.getElementById('feedback'); //Элемент, в котором будет содержаться сообщение
  // Когда поле для ввода имени пользователя получает/теряет фокус,
  // вызываем одну из функций, записанных выше:
  el.addEventListener('focus', tipUsername, false); // Событие focus вызывает функцию tipUsername()
  el.addEventListener('blur', checkUsername, false); // Событие blur вызывает функцию checkUsername()

  // Phone number validation
  function ValidPhone() {
    let re = /^((\+?7|8)[ \-] ?)?((\(\d{3}\))|(\d{3}))?([\- ])?(\d{3}[\- ]?\d{2}[\- ]?\d{2})$/;
    let inputTel = tel.value;
    // console.log("tel:",inputTel)
    let valid = re.test(inputTel);
    console.log('telValid:', valid);
    return valid; // true or false
  } // console.log("phone:", ValidPhone())

  // email validation
  function ValidMail() {
    let re = /^[\w]{1}[\w-\.]*@[\w-]+\.[a-z]{2,5}$/i;
    let inputMail = email.value;
    let valid = re.test(inputMail);
    // console.log('mailValid:', valid);
    return valid; // true or false
  }
  // console.log("mail:", ValidMail())

  //form clear
  function clearForm(forma) {
    // let formEl = document.forms.forma;
    // name of the form
    // let elements = forma.elements;

    forma.reset();

    for (let i = 0; i < elements.length; i++) {

      let field_type = elements[i].type.toLowerCase();

      switch (field_type) {
        case "text":
        case "password":
        case "textarea":
        case "hidden":
          elements[i].value = "";
          break;
        case "radio":
        case "checkbox":
          if (elements[i].checked) {
            elements[i].checked = false;
          }
          break;
        case "select-one":
        case "select-multi":
          elements[i].selectedIndex = -1;
          break;
        default:
          break;
      }
    }
  }
  clearForm();
}